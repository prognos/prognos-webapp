# Prognos Webapp

> Prediction Market for the masses

## Description

This is frontend app using [Vue.js](https://vuejs.org/). It requires [npm](https://www.npmjs.com/) and [node](https://nodejs.org/en/) to build and run.

## Setup

You should only need to run

```bash
	$ npm install
```

to install all dependencies.

## Running

Run the development server with hot-reload

```bash
    $ npm run dev
```

then open the browser to http://localhost:1234/

## NPM tips

```bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run all tests
npm test
```

## Screenshot (June 10 2018)

![User Home page](Screenshot_20180610_234430.png)

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

## Contributing

Very Welcome!
