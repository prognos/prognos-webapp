import moment from 'moment'

const filters = [
  {
    name: 'formatDate',
    func: function(value) {
      if (value) {
        return moment(String(value)).format('MM/DD/YYYY')
      }
    }
  },
  {
    name: 'formatDateTime',
    func: function(value) {
      if (value) {
        return moment(String(value)).format('MM/DD/YYYY hh:mm:ss [GMT]Z')
      }
    }
  },
  {
    name: 'clampString',
    func: function(string, length) {
      return string.length > length
        ? string.substring(0, length - 3) + '...'
        : string
    }
  }
]

export { filters }
