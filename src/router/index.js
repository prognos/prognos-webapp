import Vue from 'vue'
import Router from 'vue-router'
import Claims from '@/components/Claims'
import User from '@/components/User'
import Login from '@/components/Login'
import Register from '@/components/Register'
import ClaimNew from '@/components/ClaimNew'
import Claim from '@/components/Claim'
import store from '@/store'

const ifNotAuthenticated = (to, from, next) => {
  if (!store.getters.isAuthenticated) {
    next()
    return
  }
  next('/')
}

const ifAuthenticated = (to, from, next) => {
  if (store.getters.isAuthenticated) {
    next()
    return
  }
  next('/login')
}

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Claims',
      component: Claims
    },
    // { path: '', component: Claims },
    { path: '/login', component: Login, beforeEnter: ifNotAuthenticated },
    { path: '/register', component: Register, beforeEnter: ifNotAuthenticated },
    {
      path: '/user/:id',
      component: User
    },
    {
      path: '/claim/new',
      beforeEnter: ifAuthenticated,
      component: ClaimNew
    },
    {
      path: '/claim/:id',
      component: Claim
    }
  ]
})
