import Vue from 'vue'
import Vuex from 'vuex'

import { auth } from './types.js'
// import axi from '../axi'
import axi from '@/axi'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token: sessionStorage.getItem(auth.STORAGE_TOKEN_KEY),
    authUser: JSON.parse(sessionStorage.getItem(auth.STORAGE_USER_KEY)),
    status: auth.status.NONE
  },
  getters: {
    isAuthenticated: state => !!state.token && !!state.authUser,
    authStatus: state => state.status,
    isAdmin: (state, getters) => {
      if (getters.isAuthenticated) {
        const role = state.authUser.role_id
        return role === 1
      }
      return false
    }
  },
  mutations: {
    [auth.AUTH_REQUEST]: state => {
      state.status = auth.status.LOADING
    },
    [auth.AUTH_SUCCESS]: (state, token) => {
      sessionStorage.setItem(auth.STORAGE_TOKEN_KEY, token)
      axi.defaults.headers.common['Authorization'] = token
      state.status = auth.status.SUCCESS
      state.token = token
    },
    [auth.AUTH_ERROR]: (state, error) => {
      console.log('Auth Error: ' + error)
      sessionStorage.removeItem(auth.STORAGE_TOKEN_KEY)
      state.status = auth.status.ERROR
    },
    [auth.AUTH_LOGOUT]: state => {
      sessionStorage.removeItem(auth.STORAGE_TOKEN_KEY)
      sessionStorage.removeItem(auth.STORAGE_USER_KEY)
      // remove the axios default header
      delete axi.defaults.headers.common['Authorization']
      state.status = auth.status.NONE
      state.token = ''
      state.authUser = null
    },
    [auth.USER_SUCCESS]: (state, user) => {
      sessionStorage.setItem(auth.STORAGE_USER_KEY, JSON.stringify(user))
      state.authUser = user
    },
    [auth.USER_ERROR]: (state, error) => {
      console.log('User Request Error: ' + error)
      state.authUser = null
      state.commit(auth.AUTH_ERROR, error)
    }
  },
  actions: {
    [auth.AUTH_REQUEST]: ({ commit, dispatch }, user) => {
      commit(auth.AUTH_REQUEST)

      return axi
        .post('auth/login', user, {
          headers: { 'Content-Type': 'application/json' }
        })
        .then(resp => {
          const token = resp.data.token

          commit(auth.AUTH_SUCCESS, token)
          dispatch(auth.USER_REQUEST)
        })
        .catch(err => {
          commit(auth.AUTH_ERROR, err)
        })
    },
    [auth.AUTH_REGISTER]: ({ commit, dispatch }, user) => {
      commit(auth.AUTH_REQUEST)

      return axi
        .post('auth/signup', user, {
          headers: { 'Content-Type': 'application/json' }
        })
        .then(resp => {
          const token = resp.data.token
          const user = resp.data.user

          commit(auth.AUTH_SUCCESS, token)
          commit(auth.USER_SUCCESS, user)
        })
        .catch(err => {
          commit(auth.AUTH_ERROR, err)
        })
    },
    [auth.USER_REQUEST]: ({ commit, dispatch }, user) => {
      return axi
        .get('auth/user')
        .then(resp => {
          const user = resp.data

          commit(auth.USER_SUCCESS, user)
        })
        .catch(err => {
          commit(auth.USER_ERROR, err)
        })
    }
  }
})
