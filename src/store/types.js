const auth = {
  STORAGE_TOKEN_KEY: 'auth-token',
  STORAGE_USER_KEY: 'auth-user',
  AUTH_REQUEST: 'auth-request',
  AUTH_REGISTER: 'auth-register',
  AUTH_SUCCESS: 'auth-success',
  AUTH_ERROR: 'auth-error',
  AUTH_LOGOUT: 'auth-logout',
  USER_REQUEST: 'user-request',
  USER_ERROR: 'user-error',
  USER_SUCCESS: 'user-success',

  status: {
    SUCCESS: 'success',
    LOADING: 'loading',
    ERROR: 'error',
    NONE: 'none'
  }
}

export { auth }

export default {
  auth
}
