// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from '@/App'
import router from '@/router'
import store from './store'
import Vuetify from 'vuetify'
import Vuex from 'vuex'
import 'vuetify/dist/vuetify.min.css'

import axi from '@/axi'
import { filters } from '@/filters'
import colors from 'vuetify/es5/util/colors'
import AsyncComputed from 'vue-async-computed'

// Vue Async
Vue.use(AsyncComputed)

// Vuetify
Vue.use(
  Vuetify,
  {
    theme: {
      primary: colors.red.darken3,
      secondary: colors.grey.darken1,
      accent: colors.teal.base,
      error: colors.red.accent4,
      info: colors.blueGrey.lighten2,
      warning: colors.deepOrange.darken4
    }
  },
  Vuex
)

Vue.config.productionTip = false

// Axios Instance
Vue.prototype.$axi = axi

// Filters
for (const filter of filters) {
  Vue.filter(filter.name, filter.func)
}

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
